package com.example.boardapi.entity;

import com.example.boardapi.enums.CategoryKind;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private LocalDate creatDate;

    @Column(nullable = false, length = 15)
    private String writerName;

    @Enumerated
    @Column(nullable = false)
    private CategoryKind category;

    @Column(nullable = false, length = 30)
    private String titleName;

    @Column()
    private String boardContent;

}
