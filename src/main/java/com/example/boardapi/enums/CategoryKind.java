package com.example.boardapi.enums;


import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum CategoryKind {
    NOTICE("공지"),
    TO_DAY("오늘"),
    FREE_BOARD("자유");

    private String name;
}
